# make-it-quick
Make It Quick is a tool for manage tasks.

# Get the application up and running
* Build and start the application
```bash
$ docker-compose build
$ docker-compose up -d
```

First time, you need to create and migrate database.

* Database creation
```bash
$ docker-compose run --rm app rake db:create
```
* Database migration
```bash
$ docker-compose run --rm app rails db:migrate
# target:production (default:develop)
$ docker-compose run --rm app rails db:migrate RAILS_ENV=production
# target:test
$ docker-compose run --rm app rails db:migrate RAILS_ENV=test
```

# Information and Utilities about development

* System dependencies

* Install js packages
```bash
$ docker-compose run --rm app yarn install
```
* How to run the test suite
```bash
$ docker-compose run --rm app bin/rails test test/controllers
```
* How to run the js test suite 
```bash
$ docker-compose run --rm app yarn test
```
* How to run ESLint
```bash
$ docker-compose run --rm app yarn run eslint --ext .js,.vue app
```
* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions