Rails.application.routes.draw do
  get 'make_it_quick/index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :phases do
    resources :tasks do
      patch 'order', to: 'tasks#order'
    end
  end
  root 'make_it_quick#index'
end
