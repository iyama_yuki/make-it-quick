require "test_helper"

class MakeItQuickControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get make_it_quick_index_url
    assert_response :success
  end
end
