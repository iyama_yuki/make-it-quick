import { shallowMount } from '@vue/test-utils';
import CardHeader from 'components/card_header.vue';

test('title shuld be displayed', () => {
    const title = "テスト";
    const wrapper = shallowMount(CardHeader, { propsData: { title } })
    const div = wrapper.find('.d-flex');
    expect(div.text()).toBe(title);
})