class Phase < ApplicationRecord
    has_many :tasks, -> { order(order: :asc) }
end
