class PhasesController < ApplicationController
    def create
        @phase = Phase.new(phase_params)

        @phase.save
        redirect_to make_it_quick_index_path
    end

    def update      
        task_params = params[:phase][:tasks]
        task_params.each_with_index do |task_param,index|
            task = Task.find(task_param[:id])
            task.update(phase_id: params[:id],
            order: index)
        end
    end

    private
    def phase_params
        params.require(:phase).permit(:title)
    end
end
