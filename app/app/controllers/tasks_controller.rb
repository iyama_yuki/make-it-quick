class TasksController < ApplicationController
    def create
        @phase = Phase.find(params[:phase_id])
        @task = @phase.tasks.create(task_params)
        redirect_to make_it_quick_index_path
    end
    
    def update
        @task = Task.find(params[:id])
        @task.update(task_params)
        redirect_to make_it_quick_index_path
    end

    private
    def task_params
        params.require(:task).permit(:title)
    end
end
